/**
 * Created by wdydlyb on 25.11.21.
 */

trigger CaseSolutionTrigger on Case_Solution__c (after update) {

    for (Case_Solution__c n : Trigger.new){
        Case_Solution__c old = Trigger.oldMap.get(n.Id);

        if(n.Solution_Results__c != old.Solution_Results__c){
            if(n.Solution_Results__c == 'Resolved via call' || n.Solution_Results__c == 'Resolved in advance'){
                List<Case_Solution__c> relatedCaseSolutions =  n.Case__r.Case_Solutions__r;
                n.Case__r.Status = 'Resolved';
                for(Case_Solution__c relatedCaseSolution : relatedCaseSolutions){
                    relatedCaseSolution.Solution_Results__c = 'Cancelled';
                }
            }
        }
    }
}